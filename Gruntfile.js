module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: ';'
                },
                dist: {
                    // the files to concatenate
                    src: [
                        'bower_components/angular/angular.js',
                        'bower_components/angular-route/angular-route.js',
                        'bower_components/angular-sanitize/angular-sanitize.js',
                        'bower_components/ng-file-upload/ng-file-upload-all.min.js',
                        'app/backend/static/js/app/app.js',
                        'app/backend/static/js/app/controllers/**/*.js',
                        'app/backend/static/js/app/directives/**/*.js',
                        'app/backend/static/js/app/services/**/*.js'
                    ],
                    // the location of the resulting JS file
                    dest: 'app/backend/static/all.js'
                }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            app: ['Gruntfile.js', 'app/**/*.js']
        },
        watch: {
            options: {
                livereload: true,
                nospawn: true
            },
            app: {
                files: ['<%= jshint.app %>'],
                tasks: ['jshint', 'concat', 'develop']
            },
        },
        develop: {
            server: {
                file: 'app/boot.js',
                env: { NODE_ENV: 'development' }
            }
        }
    });

    grunt.loadNpmTasks('grunt-develop');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('default', ['jshint', 'concat', 'develop', 'watch']);

};
