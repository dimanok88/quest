let mongoose = require('mongoose');
let uuid = require('node-uuid');

let schema = new mongoose.Schema({
    id: {
        type: String,
        lowercase: true,
        trim: true,
        default: function() {
            return uuid.v4();
        }
    },
    uri: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
        trim: true,
        validate: {
            validator: function(v) {
                return /[\w\-]/.test(v);
            },
            message: '{VALUE} is not a valid uri!'
        },
    },
    title: {
        type: String,
        required: true,
        trim: true
    },
    metaTitle: {
        type: String,
        trim: true
    },
    metaDescription: {
        type: String,
        trim: true
    },
    dateCreate: {
        type: Date,
        default: Date.now
    },
    dateUpdate: {
        type: Date,
        default: Date.now
    },
    text: String,
}, { strict: true });

//middelware for Save pages
schema.pre('save', function(next) {
    this.title = this.title.replace(/(<([^>]+)>)/ig, '');
    this.uri = this.uri.replace(/(<([^>]+)>)/ig, '');
    //this.metaTitle = this.metaTitle.replace(/(<([^>]+)>)/ig, '');
    next();
});

let Pages = mongoose.model('Pages', schema);

module.exports = Pages;
