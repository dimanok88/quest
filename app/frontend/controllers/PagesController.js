let PagesModel = require('../../models/PagesModel');

module.exports.controller = function(app) {

    app.get('/p/:uri', function(req, res, next) {
        PagesModel.findOne({uri: req.params.uri}, function(err, result) {
            if (err) {
                return res.error(500);
            }
            if (!result) {
                return res.error(404);
            }
            res.success('pages/index',{
                pageInfo: result,
                menu: []
            });
        });
    });
    app.post('/pages/', function(req, res, next) {
        var page = new PagesModel(req.body);
        page.save(function(err) {
            if (err) {
                return res.send(500);
            }
            res.send(page);
        });
    });
};
