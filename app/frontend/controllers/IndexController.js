let PagesModel = require('../../models/PagesModel');

module.exports.controller = function(app) {

    app.get('/', function(req,res,next){
        var page = PagesModel.findOne({uri: "main"}, function(err, result){
            if(err){
                return res.error(500);
            }
            return res.success('index', {page: result || []});
        });
    });
};
