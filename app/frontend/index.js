var express = require('express'),
    http = require('http'),
    path = require('path'),
    front = express(),
    engine = require('ejs-locals'),
    fs = require('fs');

front.use(express.static(path.join(__dirname, 'public')));

front.engine('ejs', engine);
front.set('views', __dirname + '/views');
front.set('view engine', 'jade');

require('./../middleware/addMenu')(front);

//include all controllers from folder
fs.readdirSync('./app/frontend/controllers').forEach(function(file) {
    if (file.substr(-3) === '.js') {
        var route = require('./controllers/' + file);
        route.controller(front);
    }
});

module.exports = front;
