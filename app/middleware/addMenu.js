let PagesModel = require('../models/PagesModel');

module.exports = function(app) {
    app.use(function(req, res, next) {
        PagesModel.find({uri: {$not: /main/} }, {_id: 0, uri: 1, title: 1}, function(err, results) {
            if (err) {
                return res.error(500);
            }
            res.locals.globalMenu = results || [];
            console.log(results);
            next();
        });
    });
};