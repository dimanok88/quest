var errorList = require('../config/errors.json');
var _ = require('underscore');

module.exports = function(req, res, next) {
    res.success = function(view, results) {
        res.render(view, _.extend(res.locals || {}, results || {}));
    };
    res.error = function(code, text) {
        res.status(errorList[code] ? errorList[code].code : errorList[404].code);
        res.render('error', {
            code: errorList[code] ? errorList[code].code : errorList[404].code,
            message: errorList[code] ? errorList[code].message : errorList[404].message,
            text: text
        });
    };
    next();
};
