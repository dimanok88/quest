var express = require('express'),
    http = require('http'),
    path = require('path'),
    app = express(),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    methodOverride = require('method-override'),
    expressSession = require('express-session'),
    mongooseSession = require('mongoose-session'),
    async = require('async'),
    uuid = require('node-uuid'),
    crypto = require('crypto'),
    MongoStore = require('connect-mongo')(expressSession),
    frontend = require('./frontend'),
    backend = require('./backend/admin'),
    response = require('./middleware/response');

var config = require('./config/local');

app.set('trust proxy', true);
app.set('json spaces', config.app.jsonSpaces || 0);

var runApp = function() {
    http.createServer(app).listen(config.app.port, function() {
        console.log('Express server listening on port ' + config.app.port);
    });
};

// database connection
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
db = mongoose.connect(config.mongodb.app, function(err) {
    if (err) {
        return console.error(err);
    }
    runApp();
});

app.use(response);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cookieParser());
app.use(expressSession({
    secret: config.cookie.secret,
    resave: true,
    saveUninitialized: true,
    store: new MongoStore({
        mongooseConnection: mongoose.connection
    })
}));

app.use('/admin', backend);
app.use('/', frontend);
