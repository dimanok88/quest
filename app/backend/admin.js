var exp = require('express');
var fs = require('fs');
var path = require('path');
var admin = exp();
var ejs = require('ejs-locals');

admin.engine('ejs', ejs);
admin.use('/', exp.static(__dirname + '/static'));
admin.set('views', __dirname + '/views');
admin.set('view engine', 'ejs');

//include all controllers from folder
fs.readdirSync('./app/backend/controllers').forEach(function(file) {
    if (file.substr(-3) === '.js') {
        var route = require('./controllers/' + file);
        route.controller(admin);
    }
});

admin.use(function(req, res) {
    res.error(404);
});

module.exports = admin;
