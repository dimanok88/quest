let PagesModel = require('../../models/PagesModel');
let path = "/pages";

module.exports.controller = function(app) {

    app.get(path, function(req, res, next) {
        PagesModel.find({}, function(err, results) {
            if (err) {
                return res.error(500);
            }
            if (!results) {
                return res.error(404);
            }
            res.json(results);
        });
    });

    app.get(path + '/:id', function(req, res, next) {
        PagesModel.findOne({ id: req.params.id }, function(err, result) {
            if (err) {
                return res.error(500);
            }
            if (!result) {
                return res.error(404);
            }
            res.json(result);
        });
    });

    app.post(path + '/:id', function(req, res, next) {
                PagesModel.findOneAndUpdate({ id: req.params.id }, {
                    $set: {
                        title: req.body.title,
                        text: req.body.text,
                    }
                },
                {
                    runValidators: true,
                },
                function(err, doc) {
                    console.log(err);
                    if (err) {
                        return res.status(500).json(err.errors);
                    }
                    res.send(doc);
                });
        });

};
