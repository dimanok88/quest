angular
    .module('services')
    .service('$validation', function() {
        /* @TODO validation */
        return {
            title: function(value) {
                return !!value ? {
                    success: true
                } : {
                    error: {
                        message: 'Path `title` is required.'
                    }
                };
            }
        };
    });
