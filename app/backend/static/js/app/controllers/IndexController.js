angular
    .module('controllers')
    .controller('IndexController', function($scope, $http){
        $http
            .get('/admin/pages')
            .success(function(res) {
                $scope.pagesList = res;
            })
            .error(function(err) {
                $scope.error = err;
            });
    });