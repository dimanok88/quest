angular
    .module('controllers')
    .controller('PageEditController', function($scope, $http, $routeParams) {
        $http
            .get('/admin/pages/' + $routeParams.id)
            .success(function(res) {
                $scope.values = res;
            })
            .error(function(err) {
                $scope.error = err;
            });
        $scope.actions = {
            save: function() {
                $http
                    .post('/admin/pages/' + $routeParams.id, $scope.page)
                    .success(function(res) {
                        $scope.message = 'Saved';
                    })
                    .error(function(err) {
                        $scope.error = err;
                    });
            }
        };

        $scope.states = {};

        $scope.isSuccess = function() {
            for (var i = 0; i < $scope.states.length; i++) {
                if ($scope.states[i] !== 'success') {
                    return false;
                }
            }
        };

        $http.get('/admin/js/app/forms/pageForm.json').then(function(result) {
            $scope.myEntity = result.data;
            for (var i = 0; i < result.data.fields.length; i++) {
                if (result.data.fields[i].type !== 'button') {
                    $scope.states[result.data.fields[i].name] = '';
                }
            }
        });
    });
