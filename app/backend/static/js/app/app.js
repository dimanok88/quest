var pathLink = '/admin/js/app/tpl';

angular.module('app', [
    'directives',
    'services',
    'filters',
    'controllers',
    'ngRoute',
    'ngSanitize'
]);

angular.module('directives', []);
angular.module('services', []);
angular.module('filters', []);
angular.module('controllers', []);


angular.module('app').config(function($routeProvider){
    $routeProvider
        .when('/', {
            templateUrl: pathLink+'/index.html',
            controller: 'IndexController'
        })
        .when('/pages/:id', {
            templateUrl: pathLink+'/pages/edit.html',
            controller: 'PageEditController'
        })
        .otherwise({
            redirectTo: '/'
        });
});