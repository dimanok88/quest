angular
    .module('directives')
    .directive('topNav', function() {
        return {
            replace: true,
            templateUrl: '/admin/js/app/tpl/directives/nav.html'
        };
    });
