angular
    .module('directives')
    .directive('formGenerate', function() {
        return {
            replace: true,
            scope: {
                entity: '=',
                values: '='
            },
            templateUrl: '/admin/js/app/tpl/directives/form.html'
        };
    });
