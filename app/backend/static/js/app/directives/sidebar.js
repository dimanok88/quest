angular
    .module('directives')
    .directive('sideBar', function() {
        return {
            replace: true,
            templateUrl: '/admin/js/app/tpl/directives/sidebar.html'
        };
    });
