angular
    .module('directives')
    .directive('control', function() {
        return {
            replace: true,
            scope: {
                action: '=',
                error: '=',
                value: '=',
                state: '=',
                title: '=',
                name: '=',
                type: '='
            },
            templateUrl: '/admin/js/app/tpl/directives/control.html',
            controller: function($scope, $validation) {
                // $scope.$watch('value', function(value) {
                    // var valid = $validation.title(value);
                    // if (valid.success) {
                    //     $scope.success = true;
                    //     $scope.error = false;
                    // } else {
                    //     $scope.success = false;
                    //     $scope.error = valid.error.message;
                    // }
                // });
            }
        };
    });
